import React, { Fragment, useState, useEffect, useCallback } from "react";

import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
import Spinner from "../../components/UI/Spinner/Spinner";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import { useSelector, useDispatch } from "react-redux";
import axios from "../../axios-orders";
import * as actions from '../../store/actions/index'

const BurgerBuilder = props => {
  const [ purchasing, setPurchasing ] = useState(false);

  const dispatch = useDispatch();

  const onUpdateIngredients = (ingredientType, isAdded) => dispatch(actions.updateIngredients(ingredientType, isAdded));
  const onInitIngredients = useCallback(() => dispatch(actions.initIngredients()), [dispatch]);
  const onInitPurchase = () => dispatch(actions.purchaseInit());
  const onSetAuthRedirectPath = (path) => dispatch(actions.setAuthRedirectPath(path));

  const ingredients = useSelector(state => state.burgerBuilder.ingredients);
  const totalPrice = useSelector(state => state.burgerBuilder.totalPrice);
  const error = useSelector(state => state.burgerBuilder.error);
  const isAuth = useSelector(state => state.auth.token !== null);

  useEffect(()=>{
    onInitIngredients();
  }, [onInitIngredients])

  const updatePurchaseState = () => {
    const sum = Object.values(ingredients).reduce((sum, cur) => sum + cur, 0)
    
    return sum > 0
  }

  const purchaseHandler = () => {
    if (isAuth) { 
      setPurchasing(true) 
    } else {
      onSetAuthRedirectPath('/checkout');
      props.history.push('/auth');
    }
  }

  const purchaseCancelHandler = () => {
    setPurchasing(false)
  }

  const purchaseContinueHandler = () => {
    onInitPurchase();
    props.history.push('/checkout');
  }

  const getIngredients = () => {
    const disabledInfo = {
      ...ingredients
    };

    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0
    }

    if (ingredients) {
      return (
        <Fragment>
          <Burger ingredients={ingredients}/>
          <BuildControls 
            price = {totalPrice.toFixed(2)}
            ingredientsUpdated = {onUpdateIngredients }
            purchasable = {updatePurchaseState()}
            ordered={purchaseHandler}
            isAuth={isAuth}
            disabled = {disabledInfo} />
        </Fragment>
      );
    }

    if (error) {
      return <p>Ingredients can't be loaded!</p>
    } else {
      return <Spinner />
    }
  }

  const getOrderSummary = () => {
    if (!ingredients) {
      return null;
    }

    return (
      <OrderSummary 
        ingredients = {ingredients} 
        purchaseCanceled={purchaseCancelHandler} 
        purchaseContinued = {purchaseContinueHandler}
        price={totalPrice.toFixed(2)}
      />
    );
  }

  return (
    <Fragment>        
      <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
        { getOrderSummary() }
      </Modal>
      { getIngredients() }
    </Fragment>
  )
}

export default withErrorHandler(BurgerBuilder, axios);
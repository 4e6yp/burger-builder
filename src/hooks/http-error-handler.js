import {useState, useEffect} from 'react';

export default httpClient => {
  const [error, setError] = useState(null)

  const { interceptors } = httpClient;
    
  const requestInterceptor = interceptors.request.use(req => {
    setError(null);
    return req;
  });

  const responseInterceptor = interceptors.response.use(res => res, err => {
    setError(err);
  });

  const errorConfirmedHandler = () => {
    setError(null);
  }
  
  useEffect(() => {
    return () => {
      interceptors.request.eject(requestInterceptor);
      interceptors.response.eject(responseInterceptor);
    }
  }, [requestInterceptor, responseInterceptor, interceptors])

  return [error, errorConfirmedHandler]
}
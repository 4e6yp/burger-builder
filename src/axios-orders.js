import axios from "axios";

const instance = axios.create({
  baseURL: 'https://burger-app-8c417.firebaseio.com/'
});

export default instance;
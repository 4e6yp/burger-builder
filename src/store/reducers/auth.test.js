import reducer, { initialState } from './auth';
import * as actionTypes from '../actions/actionTypes'

describe('auth reducer', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  it('should store token on login', () => {
    expect(reducer(initialState, { 
      type: actionTypes.AUTH_SUCCESS, 
      idToken: 'token', 
      userId: 'userId'
    })).toEqual({
      token: 'token',
      userId: 'userId',
      error: null,
      loading: false,
      authRedirectPath: '/'
    })
  })
})
import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../../shared/utility'

const initialState = {
  ingredients: null,
  totalPrice: 4,
  error: false,
  isBuilding: false,
}

const upgradeIngredientReducer = (state, action) => {
  const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.2,
    bacon: 0.7
  }

  const multiplier = action.isAdded ? 1 : -1;
  const updatedIngredient = {[action.ingredientType]: state.ingredients[action.ingredientType] + 1 * multiplier}
  const updatedIngredients = updateObject(state.ingredients, updatedIngredient)
  
  const updatedState = {
    ingredients: updatedIngredients,
    totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingredientType] * multiplier,
    isBuilding: true
  }
  
  return updateObject(state, updatedState);
}

const setIngredients = (state, action) => {
  return updateObject(state, {
    ingredients: action.ingredients,
    totalPrice: initialState.totalPrice,
    error: false,
    isBuilding: false,
  })
}

const fetchIngredientsFailed = (state, action) => {
  return updateObject(state, { error: true });
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.UPDATE_INGREDIENTS: return upgradeIngredientReducer(state, action);
    case actionTypes.SET_INGREDIENTS: return setIngredients(state, action);
    case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailed(state, action);
  
    default:
      return state;
  }  
}

export default reducer;
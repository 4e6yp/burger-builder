import * as actionTypes from './actionTypes';

export const updateIngredients = (ingredientType, isAdded) => {
  return {
    type: actionTypes.UPDATE_INGREDIENTS,
    ingredientType: ingredientType,
    isAdded: isAdded
  }
}

export const setIngredients = (ingredients) => {
  return {
    type: actionTypes.SET_INGREDIENTS,
    ingredients: ingredients
  }
}

export const fetchIngredientsFailed = () => {
  return {
    type: actionTypes.FETCH_INGREDIENTS_FAILED
  }
}

export const initIngredients = () => {
  return {
    type: actionTypes.INIT_INGREDIENTS
  }
}
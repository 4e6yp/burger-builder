import React from 'react';

import classes from './BuildControls.module.css'
import BuildControl from './BuildControl/BuildControl'

const controls = [
  { label: 'Salad', type: 'salad' },
  { label: 'Bacon', type: 'bacon' },
  { label: 'Cheese', type: 'cheese' },
  { label: 'Meat', type: 'meat' },
];

const buildControls = (props) => (
  <div className={classes.BuildControls}>
    <p>Current Price: <strong>{props.price}</strong></p>
    { controls.map(c => (
      <BuildControl 
        key={c.label} 
        label={c.label} 
        added={() => props.ingredientsUpdated(c.type, true)} 
        removed={() => props.ingredientsUpdated(c.type, false)}
        disabled={props.disabled[c.type]}
        />
    )) }
    <button 
      className = {classes.OrderButton} 
      onClick={props.ordered}
      disabled = {!props.purchasable}>{props.isAuth ? "ORDER NOW" : "SIGN UP TO ORDER"}</button>
  </div>
);

export default buildControls;